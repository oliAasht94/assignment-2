﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace Assignment2
{
    public partial class Form1 : Form
    {

        SqlCeConnection mySqlConnection;

        public Form1()
        {
            InitializeComponent();
            populateListBox();
        }


        public void populateListBox()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=C:\temp\Users.sdf ");

            String selcmd = "SELECT username, password, email, user_type FROM Users ORDER BY username";

            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

        
 
       	try
       	{
           	mySqlConnection.Open();
 
           	SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
 
           	lbUsers.Items.Clear();
 
           	while (mySqlDataReader.Read())
           	{

                lbUsers.Items.Add(mySqlDataReader["username"] + " " +
                        mySqlDataReader["password"] + mySqlDataReader["email"] + mySqlDataReader["user_type"]);
 
 
           	}
         	}
 
       	catch (SqlCeException ex)
       	{
 
         	  MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
       	}
 
    	}
 
 
 
    	/*public void cleartxtBoxes()
    	{
        	txtId.Text = txtName.Text = txtAddress.Text = "";
    	}*/
 
   public bool checkInputs()
    	{
        	bool rtnvalue = true;

            if (string.IsNullOrEmpty(registerUserName.Text) ||
            string.IsNullOrEmpty(registerPassword.Text) ||
            string.IsNullOrEmpty(registerEmail.Text) ||
                string.IsNullOrEmpty(regUserType.Text))
        	{
            	MessageBox.Show("Error: Please check your inputs");
            	rtnvalue = false;
        	}
 
        	return (rtnvalue);
 
    	}
 
 
   public void insertRecord(String Username, String Password, String Email, String UserType,  String commandString)
    	{
 
        	try
  	      {
            	SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);
 
                cmdInsert.Parameters.AddWithValue("@username", Username);
                cmdInsert.Parameters.AddWithValue("@password", Password);
            	cmdInsert.Parameters.AddWithValue("@email", Email);
                cmdInsert.Parameters.AddWithValue("@user_type", UserType);
            	cmdInsert.ExecuteNonQuery();
        	}
        	catch (SqlCeException ex)
        	{
            	MessageBox.Show(Username + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        	}
 
    	}

 
    	
    



        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                String commandString = "INSERT INTO Users(username, password, email, user_type) VALUES (@username, @password, @email, @user_type)";

                insertRecord(registerUserName.Text, registerPassword.Text, registerEmail.Text, regUserType.Text, commandString);
                MessageBox.Show("You have sucessfully registered!");
               populateListBox();
                //cleartxtBoxes();
            }
 
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void regUserType_TextChanged(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            String username = loginUserName.Text;
            String password = loginPassword.Text;

            /*mySqlConnection =
                   new SqlCeConnection(@"Data Source=C:\temp\Users.sdf ");*/

            String selcmd2 = "SELECT * FROM Users WHERE username = '" + username + "' AND password = '" + password + "'";

            SqlCeCommand mySqlCommand2 = new SqlCeCommand(selcmd2, mySqlConnection);
           SqlCeDataReader reader = mySqlCommand2.ExecuteReader();

           int count = 0;
            while(reader.Read()){

                count = count + 1;
            }
            if(count == 1){
                MessageBox.Show("You have successfully logged in");

                Form2 tt = new Form2();
                tt.Show();

              
            }
            /*if (count > 1)
            {
                MessageBox.Show("You have successfully logged in");
            }*/else 
            {
                MessageBox.Show("Username and password is not correct");
            }

        }
    }
}
