﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace Assignment2
{
    public partial class Form2 : Form
    {
        SqlCeConnection mySqlConnection;
        DataTable dbT = new DataTable();


        public Form2()
        {
            InitializeComponent();
            populateListBox();
        }

        public void populateListBox()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=C:\temp\Users.sdf ");

            String selcmd = "SELECT * FROM bug_reports ORDER BY bug_id";

            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                listBox1.Items.Clear();

                while (mySqlDataReader.Read())
                {

                   listBox1.Items.Add(mySqlDataReader["bug_id"] + " " +
                         mySqlDataReader["bug_description"] + mySqlDataReader["action"]);


                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
 


        public bool checkInputs()
        {
            
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(bug_id.Text) ||
                string.IsNullOrEmpty(bug_desc.Text) ||
                string.IsNullOrEmpty(assigned_pm.Text) ||
                string.IsNullOrEmpty(action.Text) ||
                string.IsNullOrEmpty(status.Text) ||
                string.IsNullOrEmpty(assigned_dev.Text))
            {
                MessageBox.Show("Error: Please complete each field");
                rtnvalue = false;
            }

            return (rtnvalue);

        }


        public void insertRecord(String Bug_ID, String Bug_desc, String Assign_PM, String Action, String Status, String Assign_Dev, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@bug_id", Bug_ID);
                cmdInsert.Parameters.AddWithValue("@bug_description", Bug_desc);
                cmdInsert.Parameters.AddWithValue("@project_manager", Assign_PM);
                cmdInsert.Parameters.AddWithValue("@action", Action);
                cmdInsert.Parameters.AddWithValue("@status", Status);
                cmdInsert.Parameters.AddWithValue("@developers", Assign_Dev);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(Bug_ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
 

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                String commandString = "INSERT INTO bug_reports(bug_id, bug_description, project_manager, action, status, developers) VALUES (@bug_id, @bug_description, @project_manager, @action, @status, @developers)";

                insertRecord(bug_id.Text, bug_desc.Text, assigned_pm.Text, action.Text, status.Text, assigned_dev.Text, commandString);
               MessageBox.Show("You have sucessfully created a bug report.");
                populateListBox();
                data();
                //cleartxtBoxes();
            }
 
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'usersDataSet.bug_reports' table. You can move, or remove it, as needed.
            this.bug_reportsTableAdapter.Fill(this.usersDataSet.bug_reports);

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        void data() // adds table to datagridView
        {

            dbT = new DataTable();

            String selcmd = "SELECT * FROM bug_reports ORDER BY bug_id";

            SqlCeDataAdapter cn = new SqlCeDataAdapter(selcmd, mySqlConnection);

            cn.Fill(dbT);

            dataGridView1.DataSource = dbT;
        }

       

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                textBox4.Text = row.Cells[0].Value.ToString();
                textBox3.Text = row.Cells[1].Value.ToString();
                comboBox2.Text = row.Cells[2].Value.ToString();
                textBox2.Text = row.Cells[3].Value.ToString();
                comboBox1.Text = row.Cells[4].Value.ToString();
                textBox1.Text = row.Cells[5].Value.ToString();

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

           // if (checkInputs())
           // {

                String commandString = "DELETE FROM bug_reports WHERE bug_id = @bug_id AND bug_description = @bug_description AND project_manager = @project_manager AND action =  @action AND status = @status AND developers = @developers ";

                insertRecord(textBox4.Text, textBox3.Text, comboBox2.Text, textBox2.Text, comboBox1.Text, textBox1.Text, commandString);
                data();

                MessageBox.Show("Bug report deleted");
                //  }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

    }
}
