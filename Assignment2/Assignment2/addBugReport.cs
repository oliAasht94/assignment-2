﻿/* 
 * addBugReport.cs - Bug tracking system
 * Copyright (c) Oliver Aasht. All Rights Reserved.
 * 01/12/2014 - 26/01/2015 
 * @version 1.1
 * @author: Oliver Aasht
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace Assignment2
{
    public partial class addBugReport : Form
    {
        SqlCeConnection mySqlConnection;

        public addBugReport()//constructor
        {
            InitializeComponent();
            mySqlConnection =
                 new SqlCeConnection(@"Data Source=C:\temp\Users.sdf ");
            mySqlConnection.Open();//opens connection to users db
            fillCombo();//fills comboBox named "assigned_pm" with the usernames of project managers 
        }

        void fillCombo()//retrieves the usernames of project managers located in the users database table
        {
            String Query = "SELECT * FROM users WHERE user_type = 'Project Manager'";
            SqlCeCommand mySqlCommand = new SqlCeCommand(Query, mySqlConnection);
            SqlCeDataReader myReader;
            try
            {
                myReader = mySqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    string sName = myReader.GetString(0);
                    assigned_pm.Items.Add(sName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void button1_Click(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        private void label5_Click(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        private void label4_Click(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        private void label1_Click(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        private void label3_Click(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        private void label5_Click_1(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        private void assigned_dev_TextChanged(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        public void insertRecord(String Bug_ID, String Bug_desc, String Assign_PM, String Action, String Status, String Assign_Dev, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@bug_id", Bug_ID);
                cmdInsert.Parameters.AddWithValue("@bug_description", Bug_desc);
                cmdInsert.Parameters.AddWithValue("@project_manager", Assign_PM);
                cmdInsert.Parameters.AddWithValue("@action", Action);
                cmdInsert.Parameters.AddWithValue("@status", Status);
                cmdInsert.Parameters.AddWithValue("@developers", Assign_Dev);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(Bug_ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button1_Click_1(object sender, EventArgs e)//Create bug report button
        {

            if (Variables.utype == "Project Manager")//Only project managers can create bug reports 
            {
                String commandString = "INSERT INTO bug_reports(bug_id, bug_description, project_manager, action, status, developers) VALUES (@bug_id, @bug_description, @project_manager, @action, @status, @developers)";

                insertRecord(bug_id.Text, bug_desc.Text, assigned_pm.Text, action.Text, status.Text, assigned_dev.Text, commandString);//inserts new bug report into appropriate db tables
                MessageBox.Show("You have sucessfully created a bug report.");//confirmation message box to confirm to the user that a new bug report has been inserted
                
            }
            else 
            {
                MessageBox.Show("You are a developer and are not permitted to add bug reports");
            }

        }
    }
}
