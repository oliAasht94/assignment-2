﻿/* 
 * LoginSystem.cs - Bug tracking system
 * Copyright (c) Oliver Aasht. All Rights Reserved.
 * 01/12/2014 - 26/01/2015 
 * @version 1.1
 * @author: Oliver Aasht
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace Assignment2
{
    public partial class LoginSystem : Form
    {

        SqlCeConnection mySqlConnection;
        int count = 0;

        public LoginSystem()//constructor
        {
            InitializeComponent();
            mySqlConnection =
                   new SqlCeConnection(@"Data Source=C:\temp\Users.sdf ");
            mySqlConnection.Open();//opens a new connection to the "Users" database
        }

        private void button1_Click(object sender, EventArgs e)//Login button
        {
           
            if (string.IsNullOrEmpty(loginUserName.Text) || string.IsNullOrEmpty(loginPassword.Text))//check to see if user has typed into both text boxes
            {
                MessageBox.Show("Please enter both a username and a password.");
            }
               else
            {//start of else
                String username = loginUserName.Text;//stores input from textboxes into string variables
                String password = loginPassword.Text;

                String selcmd = "SELECT * FROM Users WHERE username = '" + username + "' AND password = '" + password + "'";
                SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
                try
                {
                    SqlCeDataReader myReader = mySqlCommand.ExecuteReader();

                    
                    while (myReader.Read())
                    {
                        count += 1;
                        Variables.utype = myReader["user_type"].ToString();//stores user type into Variables class for later use in addBugReport and showBugReports classes
                    }

                    if (count == 0)//incorrect details
                    {
                        MessageBox.Show("Username and password is not correct");
                    }  
                    else if (count == 1)//correct login details
                    {
                        MessageBox.Show("You have successfully logged in");
                        showBugReports form2 = new showBugReports();
                        form2.Show();//opens second form in the program
                        
                    }
                    else if (count > 1)//more than one of the same login details
                    {
                         MessageBox.Show("You have successfully logged in");
                         showBugReports form2 = new showBugReports();
                         form2.Show();//opens second form in the program
                         
                     }
                  
                }
                catch (SqlCeException ex)
                {

                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }//end of else

           
           
        }

        private void label3_Click(object sender, EventArgs e)//method unused
        {

        }

        private void button2_Click(object sender, EventArgs e)//register button
        {
            //if statement below makes sure inputs have been made into each registration field
            if (string.IsNullOrEmpty(registerUserName.Text) || string.IsNullOrEmpty(registerPassword.Text) || string.IsNullOrEmpty(registerEmail.Text) || string.IsNullOrEmpty(regUserType.Text))
            {
                MessageBox.Show("Please enter details into each field.");
            }
            else//inputs have been made
            {
                String commandString = "INSERT INTO Users(username, password, email, user_type) VALUES (@username, @password, @email, @user_type)";
                insertRecord(registerUserName.Text, registerPassword.Text, registerEmail.Text, regUserType.Text, commandString);//enters inputs into db
                MessageBox.Show("You have sucessfully registered!");
            }
        }

        public void insertRecord(String Username, String Password, String Email, String UserType, String commandString)//inserts data into users table
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@username", Username);
                cmdInsert.Parameters.AddWithValue("@password", Password);
                cmdInsert.Parameters.AddWithValue("@email", Email);
                cmdInsert.Parameters.AddWithValue("@user_type", UserType);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(Username + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        
    }
}
