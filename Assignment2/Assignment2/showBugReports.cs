﻿/* 
 * showBugReports.cs - Bug tracking system
 * Copyright (c) Oliver Aasht. All Rights Reserved.
 * 01/12/2014 - 26/01/2015 
 * @version 1.1
 * @author: Oliver Aasht
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace Assignment2
{
    public partial class showBugReports : Form
    {

        SqlCeConnection mySqlConnection;
        DataTable dbT = new DataTable();

        public showBugReports()//constructor
        {
            InitializeComponent();
            mySqlConnection =
                 new SqlCeConnection(@"Data Source=C:\temp\Users.sdf ");
            mySqlConnection.Open();//opens connection to users database
            fillCombo();//calls fillCombo method in order to fill comboBox2 with Project Managers in the users db table
        }

        void fillCombo() //retrives usernames of project managers stored in the database table named Users
        { 
            String Query =  "SELECT * FROM users WHERE user_type = 'Project Manager'";
            SqlCeCommand mySqlCommand = new SqlCeCommand(Query, mySqlConnection);
            SqlCeDataReader myReader;
            try {
                myReader = mySqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    string sName = myReader.GetString(0);//get username of Project Manager
                    comboBox2.Items.Add(sName);//add username of Project Manager into comboBox2
                }
            }
            catch(Exception ex){
                MessageBox.Show(ex.Message);
            }

        
        }

        private void showBugReports_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'usersDataSet.bug_reports' table. You can move, or remove it, as needed.
            this.bug_reportsTableAdapter.Fill(this.usersDataSet.bug_reports);

        }

        public void insertRecord(String Bug_ID, String Bug_desc, String Assign_PM, String Action, String Status, String Assign_Dev, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@bug_id", Bug_ID);
                cmdInsert.Parameters.AddWithValue("@bug_description", Bug_desc);
                cmdInsert.Parameters.AddWithValue("@project_manager", Assign_PM);
                cmdInsert.Parameters.AddWithValue("@action", Action);
                cmdInsert.Parameters.AddWithValue("@status", Status);
                cmdInsert.Parameters.AddWithValue("@developers", Assign_Dev);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(Bug_ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)//method unused but can't remove due to errors
        {

        }


        private void button3_Click(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        private void button2_Click(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        private void label11_Click(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        private void label10_Click(object sender, EventArgs e)//method unused but can't remove due to errors
        {

        }

        void data() // adds bug reports table into dataGridView
        {

            dbT = new DataTable();

            String selcmd = "SELECT * FROM bug_reports ORDER BY bug_id";

            SqlCeDataAdapter cn = new SqlCeDataAdapter(selcmd, mySqlConnection);

            cn.Fill(dbT);

            dataGridView1.DataSource = dbT;
        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)//once a row of data is selected from the dataGrid this method places the data into the appropriate textboxes and comboBoxes
        {
            if (e.RowIndex >= 0)
            {

                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                textBox4.Text = row.Cells[0].Value.ToString();
                textBox3.Text = row.Cells[1].Value.ToString();
                comboBox2.Text = row.Cells[2].Value.ToString();
                textBox2.Text = row.Cells[3].Value.ToString();
                comboBox1.Text = row.Cells[4].Value.ToString();
                textBox1.Text = row.Cells[5].Value.ToString();

            }
        }

        private void button3_Click_1(object sender, EventArgs e)//delete button
        {

            if (Variables.utype == "Project Manager")//only Project Managers can delete bug reports
            {
                String commandString = "DELETE FROM bug_reports WHERE bug_id = @bug_id AND bug_description = @bug_description AND project_manager = @project_manager AND action =  @action AND status = @status AND developers = @developers ";

                insertRecord(textBox4.Text, textBox3.Text, comboBox2.Text, textBox2.Text, comboBox1.Text, textBox1.Text, commandString);//deletes row of data according to the specific bug_id
                data();//refreshes dataGridView

                MessageBox.Show("Bug report deleted");
            }
            else 
            {
                MessageBox.Show("You are a developer and are not allowed to delete bug reports.");
            }

        }

        private void button2_Click_1(object sender, EventArgs e)//edit button
        {

            if (Variables.utype == "Project Manager")//only Project Managers can edit bug reports
            {
                String commandString = "UPDATE bug_reports SET bug_id = @bug_id, bug_description = @bug_description, project_manager = @project_manager, action =  @action, status = @status, developers = @developers WHERE bug_id = @bug_id";

                insertRecord(textBox4.Text, textBox3.Text, comboBox2.Text, textBox2.Text, comboBox1.Text, textBox1.Text, commandString);//edits row of data according to the specific bug_id
                data();//refreshes dataGridView

                MessageBox.Show("Bug report updated");
            }
            else
            {
                MessageBox.Show("You are a developer and are not allowed to edit bug reports.");
            }

        }

        private void button1_Click(object sender, EventArgs e)//add bug report button
        {
            addBugReport form3 = new addBugReport();
            form3.Show();//opens addBugReport class
        }

        private void button4_Click(object sender, EventArgs e)//refresh button
        {
            data();//refreshes data seen in the dataGridViews
        }

        private void button5_Click(object sender, EventArgs e)//logout button
        {
            this.Close();//logs out the user
        }
    }
}
