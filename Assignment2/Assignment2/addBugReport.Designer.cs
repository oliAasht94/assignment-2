﻿namespace Assignment2
{
    partial class addBugReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bug_id = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bug_desc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.assigned_pm = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.action = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.assigned_dev = new System.Windows.Forms.TextBox();
            this.status = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bug_id
            // 
            this.bug_id.Location = new System.Drawing.Point(84, 12);
            this.bug_id.Name = "bug_id";
            this.bug_id.Size = new System.Drawing.Size(100, 20);
            this.bug_id.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Bug ID:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // bug_desc
            // 
            this.bug_desc.Location = new System.Drawing.Point(94, 54);
            this.bug_desc.Multiline = true;
            this.bug_desc.Name = "bug_desc";
            this.bug_desc.Size = new System.Drawing.Size(328, 83);
            this.bug_desc.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Bug description:";
            // 
            // assigned_pm
            // 
            this.assigned_pm.FormattingEnabled = true;
            this.assigned_pm.Location = new System.Drawing.Point(155, 161);
            this.assigned_pm.Name = "assigned_pm";
            this.assigned_pm.Size = new System.Drawing.Size(121, 21);
            this.assigned_pm.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Assigned Project Manager:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // action
            // 
            this.action.Location = new System.Drawing.Point(67, 199);
            this.action.Multiline = true;
            this.action.Name = "action";
            this.action.Size = new System.Drawing.Size(292, 76);
            this.action.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Action:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(181, 364);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(122, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Create Bug Report";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // assigned_dev
            // 
            this.assigned_dev.Location = new System.Drawing.Point(140, 325);
            this.assigned_dev.Name = "assigned_dev";
            this.assigned_dev.Size = new System.Drawing.Size(210, 20);
            this.assigned_dev.TabIndex = 20;
            this.assigned_dev.TextChanged += new System.EventHandler(this.assigned_dev_TextChanged);
            // 
            // status
            // 
            this.status.FormattingEnabled = true;
            this.status.Items.AddRange(new object[] {
            "Bug reported",
            "Bug being fixed",
            "Bug fixed"});
            this.status.Location = new System.Drawing.Point(63, 283);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(121, 21);
            this.status.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 325);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Assigned Developers:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 283);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Status:";
            this.label5.Click += new System.EventHandler(this.label5_Click_1);
            // 
            // addBugReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 399);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.assigned_dev);
            this.Controls.Add(this.status);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.action);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.assigned_pm);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bug_desc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bug_id);
            this.Controls.Add(this.label1);
            this.Name = "addBugReport";
            this.Text = "addBugReport";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox bug_id;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox bug_desc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox assigned_pm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox action;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox assigned_dev;
        private System.Windows.Forms.ComboBox status;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}